package com.example.indira.scratchproject.city;

import android.util.Log;

import com.example.indira.scratchproject.MainActivity;

public class CityPresenterImpl implements ICityPresenter {
    private static final String TAG = "CityPresenterImpl";

    private ICityView cityView;
    private ICityContractView cityContractView;

    public CityPresenterImpl(MainActivity view) {
        this.cityView = view;
    }

    @Override
    public void showCities() {
        cityContractView = new CityContractViewImpl();
        cityContractView.getCities(cityView);
    }
}
