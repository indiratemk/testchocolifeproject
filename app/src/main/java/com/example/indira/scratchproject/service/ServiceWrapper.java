package com.example.indira.scratchproject.service;

import com.example.indira.scratchproject.model.ResponseCity;
import com.example.indira.scratchproject.util.Constant;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceWrapper {
    private static Retrofit retrofit;
    private ApiService service;

    public ServiceWrapper() {
        getRetrofit();
        service = retrofit.create(ApiService.class);
    }

    private static void getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
    }

    public Call<ResponseCity> getCities() {
        return service.getCities();
    }
}
