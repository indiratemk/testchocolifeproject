package com.example.indira.scratchproject.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseCity {
    @SerializedName("result")
    @Expose
    private List<City> cities;

    public List<City> getCities() {
        return cities;
    }
}
