package com.example.indira.scratchproject.city;

import android.util.Log;
import android.widget.Toast;

import com.example.indira.scratchproject.MainActivity;
import com.example.indira.scratchproject.model.City;
import com.example.indira.scratchproject.model.ResponseCity;
import com.example.indira.scratchproject.service.ServiceWrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityContractViewImpl implements ICityContractView {
    private static final String TAG = "CityContractViewImpl";
    
    
    private ServiceWrapper serviceInstance;

    @Override
    public void getCities(final ICityView view) {
        serviceInstance = new ServiceWrapper();
        Call<ResponseCity> responseCityCall = serviceInstance.getCities();
        responseCityCall.enqueue(new Callback<ResponseCity>() {
            @Override
            public void onResponse(Call<ResponseCity> call, Response<ResponseCity> response) {
                if (response.body() != null && response.isSuccessful()) {
                    view.showCities(response.body().getCities());
                }
            }

            @Override
            public void onFailure(Call<ResponseCity> call, Throwable t) {

            }
        });
    }
}
