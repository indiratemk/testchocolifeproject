package com.example.indira.scratchproject.service;


import com.example.indira.scratchproject.model.ResponseCity;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    String CITY = "towns";

    @GET(CITY)
    Call<ResponseCity> getCities();
}
