package com.example.indira.scratchproject.city;

import com.example.indira.scratchproject.model.City;

import java.util.List;

public interface ICityView {
    void showCities(List<City> cities);
}
