package com.example.indira.scratchproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.indira.scratchproject.city.CityAdapter;
import com.example.indira.scratchproject.city.CityPresenterImpl;
import com.example.indira.scratchproject.city.ICityPresenter;
import com.example.indira.scratchproject.city.ICityView;
import com.example.indira.scratchproject.model.City;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ICityView {
    private CityAdapter cityAdapter;
    private ICityPresenter cityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    public void init() {
        cityPresenter = new CityPresenterImpl(this);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        cityAdapter = new CityAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cityAdapter);
        cityPresenter.showCities();
    }

    @Override
    public void showCities(List<City> cities) {
        cityAdapter.setCities(cities);
    }
}
